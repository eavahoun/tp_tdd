package main.java;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class Holidays  {
	private String[] holydayLiStrings;
	private Date startRangeDate;
	private Date endRangeDate;
	Date[] dateListDates;
	DateFormat format = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);

	
	public void loadDate(String fileName) throws FileNotFoundException, DateException {
		
		List<Date> records = new ArrayList<>();
		try (Scanner scanner = new Scanner(new File(fileName));) {
		    while (scanner.hasNextLine()) {
		        records.add(getRecordFromLine(scanner.nextLine()));
		    }
		}catch (Exception e) {e.printStackTrace();		}
	}
	
	public Date getRecordFromLine(String string) throws DateException
	{
		System.out.println(string);
		String[] dataStrings =string.split(";");
		try {
			return format.parse(dataStrings[0]);
		} catch (ParseException e) {
			throw new DateException("csv format error");
		}
	}

	public Holidays()
	{
		try {
			startRangeDate= format.parse("2000/01/01");
			endRangeDate= format.parse("2025/12/26");

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setHolyDaysList(String[] holidaysList) throws DateException {
		// TODO Auto-generated method stub
		
	
		dateListDates= new Date[holidaysList.length];
		int count=0;
		Date dateActuDate;
		for (String string : holidaysList) {
			try {
				dateActuDate=format.parse(string);
				
				if (dateActuDate.before(startRangeDate) ||
						dateActuDate.after(endRangeDate))
				throw new DateException("Date out of bound");
				
				dateListDates[count]=format.parse(string);
				count++;
			} catch (Exception e) {
			
				throw new DateException("Date non valide");
				
			}
			
		}
		this.holydayLiStrings=holidaysList;
	}

	public void filteHolydays() {
		Collections.sort(Arrays.asList(dateListDates));
	}
	
	

	public String[] getDateListDates() {
	
		ArrayList<Date> list =new ArrayList<Date>();
		for (Date date : dateListDates) {
			list.add(date);
		}
		return convertDateListetoStringList(list);
	}
	


	public String[] getHoliDays_NO_Weekend() {
		
		ArrayList<Date> newListDates = new ArrayList<Date>();
		Calendar calendar = Calendar.getInstance();

		for (Date date : dateListDates) {
			calendar.setTime(date);
			if(calendar.get(Calendar.DAY_OF_WEEK)!= Calendar.SATURDAY &&
					calendar.get(Calendar.DAY_OF_WEEK)!= Calendar.SUNDAY)
			{
				newListDates.add(date);
			}
		}
		
		return convertDateListetoStringList(newListDates);
	}

	
	private String[] convertDateListetoStringList(ArrayList<Date> dateListDates2)
	{
		String[] litStrings= new String[dateListDates2.size()];
		int count=0;
		for (Date date : dateListDates2) {
			if (date!= null) litStrings[count]= format.format(date);
			count++;
		}
		return litStrings;
	}
	
	



}
