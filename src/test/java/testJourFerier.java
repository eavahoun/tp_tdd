package test.java;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.util.Date;

import org.junit.jupiter.api.Test;
import main.java.DateException;
import main.java.Holidays;

public class testJourFerier {


	String[] holidaysList ;
	Holidays holyDays= new Holidays();
	
	/** Test avec le [19999/01/01,  1998/12/12 ,  2002/01/10] **/
	
	/**
	 * Partie 1
	 * 
	 * En tant qu’utilisateur, je veux afficher les jours fériés
	 *  par ordres chronologique**/
	
	@Test
	public void testShowHoliDays() 
	{
		holidaysList = new String[] {"1999-01-01","1998/12/12", "2002/01/10"};
			
		try {
			
			holyDays.setHolyDaysList(holidaysList);
			assertTrue(false);

			
		} catch (DateException e) {
			assertTrue(true);		
			
		}
		
	
	}
	
	
	/** Test avec le [2026/05/01, 2003/06/08, 1999/07/15] **/
	
	@Test
	public void testShowHoliDays_2() 
	{
		holidaysList = new String[] {"2026/05/01","2003/06/08", "1999/07/15"};		
		try {
			
			holyDays.setHolyDaysList(holidaysList);
			assertTrue(false);

			
		} catch (DateException e) {
			assertTrue(true);		
			
		}
		
	
	}
	
	
	/** Test avec le [2020/03/15, 2020 /01/01, 2008/10/15 ] **/
	@Test
	public void testShowHoliDays_3() 
	{
		holidaysList = new String[] {"2020/03/15","2020/01/01", "2008/10/15"};
		
		String[] holidaysListExpected =new String[] {"2008/10/15","2020/01/01","2020/03/15" };
		
		try {
			
			holyDays.setHolyDaysList(holidaysList);
			holyDays.filteHolydays();
			assertArrayEquals(holidaysListExpected, holyDays.getDateListDates());
			
		} catch (DateException e) {
			assertTrue(false);		
			
		}
		
	
	}
	
	//Test avec le [2000/01/01, 2025/12/16, 2025/03/10]
	
	@Test
	public void testShowHoliDays_4() 
	{
		holidaysList = new String[] {"2000/01/01","2025/12/16", "2025/03/10"};		
		String[] holidaysListExpected =new String[] {"2000/01/01","2025/03/10","2025/12/16" };
		
		try {
			
			holyDays.setHolyDaysList(holidaysList);
			holyDays.filteHolydays();
			assertArrayEquals(holidaysListExpected, holyDays.getDateListDates());
			
		} catch (DateException e) {
			assertTrue(false);		
			
		}
	}
	
	
	//Test avec le [2026/12/29, 2024/ 10/10 , 2023/02/30]		
	@Test
	public void testShowHoliDays_5() 
	{
		holidaysList = new String[] {"2026/12/29","2024/10/10", "2023/02/30"};		
		try {
			holyDays.setHolyDaysList(holidaysList);
			assertTrue(false);
			
		} catch (DateException e) {
			assertTrue(true);				
		}
	}
	
	/**
	 * Partie 2
	 * 
	 * En tant qu’utilisateur, je veux afficher les jours 
	 * fériés qui ne tombent pas le week-end**/

	/**   Test avec le  [2020/12/26, 2020/12/25, 2020/12/24 ]**/
	@Test
	public void testShowHoliDays_NO_Weekend() 
	{
		holidaysList = new String[] {"2020/12/26","2020/12/25", "2020/12/24"};		
		String[] holidaysListExpected =new String[]{"2020/12/25", "2020/12/24"};
		
		try {
			
			holyDays.setHolyDaysList(holidaysList);
			assertArrayEquals(holidaysListExpected, holyDays.getHoliDays_NO_Weekend());
			
		} catch (DateException e) {
			assertTrue(false);		
			
		}
	}
	
	
	/**
	 * 
	 * Le systeme veut charger les  données depuis le fichier csv**/
	
	
	 //Test avec le  « file.csv »
	@Test
	public void testLoadData()
	{
		String fileName="file.csv";
		try {
			holyDays.loadDate(fileName);
			assertTrue(false);
		} catch (FileNotFoundException e) {
			assertTrue(true);
		} catch (DateException e) {
			assertTrue(false);
		}
		
	}
	
	
	// Test avec le  «jours_feries.csv »
	@Test
	public void testLoadData_2()
	{
		String fileName="jours_feries.csv";
		try {
			holyDays.loadDate(fileName);
			assertTrue(true);
		} catch (FileNotFoundException e) {
			assertTrue(false);
		} catch (DateException e) {
			assertTrue(false);
		}
		
	}
}
